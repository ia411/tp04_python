import unittest
from exo2 import nextStates, possibleMooves, swap, copyState, FillUpNextStates

class TestExo2(unittest.TestCase):

    def setUp(self):
        pass

    def test_swap(self):
        list_test = [0,1,2,3,4,5]
        swap(list_test, 1, 4)
        self.assertEqual(list_test[0], 0)
        self.assertEqual(list_test[1], 4)
        self.assertEqual(list_test[2], 2)
        self.assertEqual(list_test[3], 3)
        self.assertEqual(list_test[4], 1)
        self.assertEqual(list_test[5], 5)
        
        swap(list_test, 5, 4)
        self.assertEqual(list_test[0], 0)
        self.assertEqual(list_test[1], 4)
        self.assertEqual(list_test[2], 2)
        self.assertEqual(list_test[3], 3)
        self.assertEqual(list_test[4], 5)
        self.assertEqual(list_test[5], 1)

    def test_copyState(self):
        list_test1 = [0,1,2,3,4,5,6,7,8]
        list_test2 = copyState(list_test1)
        self.assertEqual(list_test2, [0,1,2,3,4,5,6,7,8])

    def test_FillUpNextStates(self):
        initial_state = [1,0,2,4,5,3,7,8,6]
        next_states = []
        FillUpNextStates(next_states, initial_state, 1, 4)
        self.assertEqual(next_states[0], [1,5,2,4,0,3,7,8,6])

    def test_possibleMooves(self):
        # 1 0 2
        # 4 5 3
        # 7 8 6
        initial_state = [1,0,2,4,5,3,7,8,6]
        next_states = possibleMooves(initial_state, 1)
        self.assertEqual(next_states, [[0,1,2,4,5,3,7,8,6],[1,2,0,4,5,3,7,8,6],[1,5,2,4,0,3,7,8,6]])

    def test_nextStates(self):
        initial_state = [1,0,2,4,5,3,7,8,6]
        state = [1,5,2,4,0,3,7,8,6]
        next_states = nextStates(initial_state)
        self.assertEqual(next_states, [[0,1,2,4,5,3,7,8,6],[1,2,0,4,5,3,7,8,6],[1,5,2,4,0,3,7,8,6]])
        next_states = nextStates(state)
        self.assertEqual(next_states, [[1,0,2,4,5,3,7,8,6],[1,5,2,0,4,3,7,8,6],[1,5,2,4,3,0,7,8,6],[1,5,2,4,8,3,7,0,6]])

    def tearDown(self):
        pass