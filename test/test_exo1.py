import unittest
from exo1 import nextStates, deplacer, verificationStates

class TestExo1(unittest.TestCase):

    def setUp(self):
        pass

    def test_deplacer(self):
        list_test = deplacer([3,3,1], 1, 1)
        self.assertEqual(list_test, [2,2,0])
        
        list_test = deplacer([1,3,1], 2, 0)
        self.assertEqual(list_test, [-1,3,0])
        
        list_test = deplacer([1,0,0], 0, 2)
        self.assertEqual(list_test, [1,2,1])

        list_test = deplacer([3,3,1], 1, 1)
        self.assertEqual(list_test, [2,2,0])
    
    def test_verificationStates(self):
        list_test = [[3,3,1], [3,1,0], [2,3,0], [2,1,1], [2,2,1], [1,2,0], [0,0,0]]
        list_test = verificationStates(list_test)
        self.assertEqual(list_test[0], [3,3,1])
        self.assertEqual(list_test[1], [3,1,0])
        self.assertEqual(list_test[2], [2,2,1])
        self.assertEqual(list_test[3], [0,0,0])
        list_test = [[2,2,0]]
        list_test = verificationStates(list_test)
        self.assertEqual(list_test[0], [2,2,0])
    
    def test_nextStates(self):
        list_test = nextStates([3,3,1])
        self.assertEqual(list_test[0], [3,2,0])
        self.assertEqual(list_test[1], [3,1,0])
        self.assertEqual(list_test[2], [2,2,0])
        
        list_test = nextStates([1,1,0])
        self.assertEqual(list_test[0], [1,2,1])
        self.assertEqual(list_test[1], [1,3,1])
        self.assertEqual(list_test[2], [2,2,1])
        self.assertEqual(list_test[3], [3,1,1])
    
    def test_BFS(self):
        NotImplementedError
    
    def test_DFS(self):
        NotImplementedError

    def tearDown(self):
        pass