MAX_BOAT = 2 #maximum number of person we can put in the boat
NB_INIT_M = 3 #number of missionary at the begining
NB_INIT_C = 3 #number of cannibal at the begining

initial_state = [NB_INIT_M, NB_INIT_C,1]
final_state = [0,0,0]

def nextStates(state):
    """Return all possible states considerating the parameter state as the actual state.

    IN :
    state -- the actual state

    OUT :
    list of all possible next state.

    """
    next_states = []
    for missionary in range(0, MAX_BOAT + 1):
        for cannibal in range(0, MAX_BOAT + 1):
            if (missionary + cannibal <= MAX_BOAT and (missionary != 0 or cannibal != 0)):
                next_states.append(deplacer(state, missionary, cannibal))
    return verificationStates(next_states)
        
def deplacer(state, nb_M, nb_C):
    """Return the next state where we mooved nb_M missionary and nb_C cannibal.

    !!! This function do not verify that the state returned is a valid state !!!

    IN :
    state -- the actual state
    nb_M -- the number of missionary to moove
    nb_C -- the number of cannibal to moove

    OUT :
    the state after mooving missionary and cannibal.

    """
    next_state = [0,0,0]
    if (state[2] == 1):
        next_state[0] = state[0] - nb_M ; next_state[1] = state[1] - nb_C ; next_state[2] = 0; 
        return next_state
    next_state[0] = state[0] + nb_M ; next_state[1] = state[1] + nb_C ; next_state[2] = 1; 
    return next_state

def verificationStates(states):
    """Verify that all states in list states in parameters are valid. Return the list of all valid states.

    IN :
    states -- the list of states to check

    OUT :
    valid_states -- the list of all valid states.

    """
    valid_states = []
    for state in states:
        if (state[0] >= 0 and state[0] <= NB_INIT_M and state[1] >= 0 and state[1] <= NB_INIT_C):
            if (state[2] == 1):
                if ((NB_INIT_M - state[0] >= NB_INIT_C - state[1] and NB_INIT_M - state[0] > 0) or NB_INIT_M - state[0] == 0):
                    valid_states.append(state)
            else:
                if ((state[0] >= state[1] and state[0] > 0) or state[0] == 0):
                    valid_states.append(state)
    return valid_states

def bfs(start, final):
    """Application of the Breadth-First Search algorithm.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            node_number += 1
            queue.append([state, node_number, precedent_node])
            path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "BFS : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "BFS : pas de solution trouvée"

def bfs_occurence(start, final):
    """Application of the Breadth-First Search algorithm looking at the occurrences of the nodes.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            if (not contains(path, state)):
                node_number += 1
                queue.append([state, node_number, precedent_node])
                path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "BFS occurence : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "BFS occurence : pas de solution trouvée"

def dfs(start, final):  
    """Application of the Deep-First Search algorithm.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            node_number += 1
            queue.insert(0, [state, node_number, precedent_node])
            path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "DFS : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "DFS : pas de solution trouvée"

def dfs_occurence(start, final):
    """Application of the Deep-First Search algorithm looking at the occurrences of the nodes.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            if (not contains(path, state)):
                node_number += 1
                queue.insert(0, [state, node_number, precedent_node])
                path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "DFS occurence : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "DFS occurence : pas de solution trouvée"

def findPath(queue):
    """Function to use after running DFS or BFS to recreate the solution path.

    The only parameter is a list of state associated with their node number and father node number.
    Each element of of the list should look like this [state, node_number, precedent_node]. This format
    allow us to find each node's father starting from the last item of the list which is the goals node the 
    algorythm succed to reached.

    IN :
    queue - list of all the nodes covered by the algorythme. The final state is the last item of the list.
    OUT :
    return list of state discribing the solution path.

    """
    path = [queue[-1]]
    while (path[-1][1] != 0):
        for state in queue:
            if (path[-1][2] == state[1]):
                path.append(state)
                break
    ordered_path = []
    for state in path:
        ordered_path.insert(0, state[0])
    return ordered_path

def contains(closed, state):
    """Check if a state is in the closed list.

    Item in liste should look like this [state, node_number, precedent_node]. We compare only the states.

    IN :
    queue - list of all the nodes covered by the algorythme. The final state is the last item of the list.
    OUT :
    return True if the state is already in the list and False if not.
    """
    for node in closed:
        if (node[0] == state):
            return True
    return False

if (__name__ == "__main__"):
    print(bfs([3,3,1], [0,0,0]))
    print(bfs_occurence([3,3,1], [0,0,0]))
    # print(dfs([3,3,1], [0,0,0])) infinite loop because of node redundancies
    print(dfs_occurence([3,3,1], [0,0,0]))