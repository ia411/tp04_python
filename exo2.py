BOARD_SIZE = 3

initial_state = [1,0,2,4,5,3,7,8,6]
final_state = [1,2,3,4,5,6,7,8,0]

def nextStates(state):
    """Return all possible states considerating the parameter state as the actual state.

    IN :
    state -- the actual state

    OUT :
    list of all possible next state

    """
    index = 0
    while (state[index] != 0):
        index += 1
    return possibleMooves(state, index)

def possibleMooves(state, index):
    next_states = []
    if (index >= BOARD_SIZE):
        FillUpNextStates(next_states, state, index, index - BOARD_SIZE)
    
    if (index % BOARD_SIZE > 0):
        FillUpNextStates(next_states, state, index, index - 1)
        
    if ((index + 1) % BOARD_SIZE > 0):
        FillUpNextStates(next_states, state, index, index + 1)
        
    if (index < BOARD_SIZE * (BOARD_SIZE -1)):
        FillUpNextStates(next_states, state, index, index + BOARD_SIZE)

    return next_states

def swap(list, index1, index2):
    """Swap two values in a list.

    IN :
    index1 -- index of the first value to swap
    index2 -- index of the second value to swap

    OUT :
    list[index1] contains the value of list[index2] and vice versa

    """
    tmp = list[index1]
    list[index1] = list[index2]
    list[index2] = tmp
    
def copyState(state):
    """Return a state equal to the state in parameter 

    This function is used to create new state without modifying the current state

    IN :
    state -- the state to copy

    OUT :
    copied_state -- the copied state

    """
    copied_state = [0,0,0,0,0,0,0,0,0]
    for index in range(0, BOARD_SIZE * BOARD_SIZE):
        copied_state[index] = state[index]
    return copied_state

def FillUpNextStates(next_states, state, index1, index2):
    """Add a new state based on current to the list next_state. The value at index1 and index2 of the new state have to be swaped.

    This function is used to fill up the list of state create and returned in possibleMooves.

    IN :
    next_states -- list of possible next state, where we had the new state
    state -- current state
    index1 -- first element to swap in the current state
    index2 -- second element to swap in the current state

    OUT :
    new_state -- the copy of the current state with values at index1 and index2 swapped

    """
    new_state = copyState(state)
    swap(new_state, index1, index2)
    next_states.append(new_state)

def bfs(start, final):
    """Application of the Breadth-First Search algorithm.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            node_number += 1
            queue.append([state, node_number, precedent_node])
            path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "BFS : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "BFS : pas de solution trouvée"

def bfs_occurence(start, final):
    """Application of the Breadth-First Search algorithm looking at the occurrences of the nodes.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            if (not contains(path, state)):
                node_number += 1
                queue.append([state, node_number, precedent_node])
                path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "BFS occurence : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "BFS occurence : pas de solution trouvée"

def dfs(start, final):  
    """Application of the Deep-First Search algorithm.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            node_number += 1
            queue.insert(0, [state, node_number, precedent_node])
            path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "DFS : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "DFS : pas de solution trouvée"

def dfs_occurence(start, final):
    """Application of the Deep-First Search algorithm looking at the occurrences of the nodes.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            if (not contains(path, state)):
                node_number += 1
                queue.insert(0, [state, node_number, precedent_node])
                path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "DFS occurence : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "DFS occurence : pas de solution trouvée"

def findPath(queue):
    """Function to use after running DFS or BFS to recreate the solution path.

    The only parameter is a list of state associated with their node number and father node number.
    Each element of of the list should look like this [state, node_number, precedent_node]. This format
    allow us to find each node's father starting from the last item of the list which is the goals node the 
    algorythm succed to reached.

    IN :
    queue - list of all the nodes covered by the algorythme. The final state is the last item of the list.
    OUT :
    return list of state discribing the solution path.

    """
    path = [queue[-1]]
    while (path[-1][1] != 0):
        for state in queue:
            if (path[-1][2] == state[1]):
                path.append(state)
                break
    ordered_path = []
    for state in path:
        ordered_path.insert(0, state[0])
    return ordered_path

def contains(closed, state):
    """Check if a state is in the closed list.

    Item in liste should look like this [state, node_number, precedent_node]. We compare only the states.

    IN :
    queue - list of all the nodes covered by the algorythme. The final state is the last item of the list.
    OUT :
    return True if the state is already in the list and False if not.

    """
    for node in closed:
        if (node[0] == state):
            return True
    return False

if (__name__ == "__main__"):
    print(bfs(initial_state, final_state))
    print(bfs_occurence(initial_state, final_state))
    # print(dfs(initial_state, final_state)) 
    print(dfs_occurence(initial_state, final_state))